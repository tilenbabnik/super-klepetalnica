/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if(sporocilo.search("&#9756;")){
    return $('<div style="font-weight: bold"></div>').html(sporocilo);

  } else if((sporocilo.lastIndexOf(".jpg")==sporocilo.length - 4 || sporocilo.lastIndexOf(".png")==sporocilo.length - 4 || sporocilo.lastIndexOf(".gif")==sporocilo.length - 4)){
      var pos = sporocilo.indexOf(" ");
      if(pos == "-1" && sporocilo.search("http")==0){
          return $('<div></div>').html('<div><br \/><a href=\"'+sporocilo+'\"><img src=\"' +sporocilo +'\" style=\'width:200px;margin-left:20px;\'><\/img><br \/><\/div><br \/>');
      }
      else if(sporocilo.search("http")){
        var vzdevek2 = sporocilo.slice(0, pos);
        var sporocilo2 = sporocilo.slice(pos, sporocilo.length);
        return $('<div></div>').html('<div><div>' +vzdevek2+ '<\/div><br \/><a href=\"'+sporocilo2+'\"><img src=\"' +sporocilo2 +'\" style=\'width:200px;margin-left:20px;\'><\/img><br \/><\/div>');
      }
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajKlikUporabnika(klepetApp, socket) {
  
  var sporocilo = "/zasebno \""+socket+"\" \"&#9756;\"";
  var sistemskoSporocilo;
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
}
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var trenutniNadimki = [];
var trenutniVzdeveki = [];


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var sporociloNovo = sporocilo.besedilo;
    for(var i=0;i<trenutniVzdeveki.length;i++){
      
      var string = trenutniVzdeveki[i];
      if(sporocilo.besedilo.indexOf(string+" se je preimenoval v ")==0){
        for(var j=0;j<trenutniNadimki.length;j=j+2){
          if(trenutniVzdeveki[i]==trenutniNadimki[j]){
            var nekiw = string+" se je preimenoval v ";
            var dede = sporocilo.besedilo.slice(nekiw.length);
            trenutniNadimki[j] = dede.slice(0, dede.length-1);
          }
        }
      }
      else if (sporocilo.besedilo.indexOf(string)==0){
        for(var j=0;j<trenutniNadimki.length;j=j+2){
          if(trenutniVzdeveki[i]==trenutniNadimki[j]){
            neki = sporocilo.besedilo.slice(sporocilo.besedilo.indexOf(" "),sporocilo.besedilo.length);
            sporociloNovo = trenutniNadimki[j+1]+":".concat(neki);
          }
        }
      
      }
    }
    
        

    var novElement = divElementEnostavniTekst(sporociloNovo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  var je = false;
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      trenutniVzdeveki[i]=uporabniki[i];
      for(var j=0; j<trenutniNadimki.length;j++){
        if(uporabniki[i]==trenutniNadimki[j]){
          je=true;
          break;
        }
      }
      if(je){
           $('#seznam-uporabnikov').append(divElementEnostavniTekst(trenutniNadimki[j+1]+ " ("+uporabniki[i]+")"));
        } else{
           $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
        }
     je = false;
      
    }

    $('#seznam-uporabnikov div').click(function() {
     procesirajKlikUporabnika(klepetApp, this.innerHTML);
      return false;
    });

  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
